@echo off
title AMF HEVC Converter - Quality preset
mode con: lines=15 cols=82

set "ffmpeg_path="

rem Check if ffmpeg.exe exists in the system PATH
where ffmpeg.exe >nul 2>&1
if %errorlevel% equ 0 (
    set "ffmpeg_path=ffmpeg.exe"
)

rem Check if ffmpeg.exe exists in the same folder as the script
if not defined ffmpeg_path (
    if exist "%~dp0ffmpeg.exe" (
        set "ffmpeg_path=%~dp0ffmpeg.exe"
    )
)

rem Check if ffmpeg.exe exists in the same folder as the input file
if not defined ffmpeg_path (
    if exist "%~dp1ffmpeg.exe" (
        set "ffmpeg_path=%~dp1ffmpeg.exe"
    )
)

rem If ffmpeg.exe was not found, display an error message
if not defined ffmpeg_path (
    color 0C
    echo ffmpeg.exe not found.
    echo.
    echo Download ffmpeg.exe from the following sources:
    echo https://www.gyan.dev/ffmpeg/builds/ - ffmpeg-git-full.7z
    echo https://github.com/BtbN/FFmpeg-Builds/releases - ffmpeg-master-latest-win64-gpl.zip
    pause
    exit /b
)

color 0B
echo AMF HEVC Video Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
rem Prompt for the bitrate value
set /P "bitrate=Set video bitrate (empty = 6000k): "
if "%bitrate%"=="" set "bitrate=6000"
set "maxrate=%bitrate%*1.2"
set "bufsize=%maxrate%*2"

color 0E
"%ffmpeg_path%" -hwaccel d3d11va -y -i "%~1" -c:v hevc_amf -pix_fmt yuv420p -usage:v high_quality -profile_tier:v high -quality:v quality -enforce_hrd:v true -vbaq:v true -high_motion_quality_boost_enable:v true -preanalysis:v true  -pa_activity_type:v y -pa_lookahead_buffer_depth:v 40 -pa_paq_mode:v caq -pa_caq_strength:v medium -pa_taq_mode:v 2 -rc:v vbr_peak -b:v %bitrate%k -maxrate:v %maxrate%k -bufsize:v %bufsize%k -c:a copy "%~dp1%~n1_hevc_quality.mp4"

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete.
echo.
pause