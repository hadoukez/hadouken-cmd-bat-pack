@echo off
title AVIF Image Converter
mode con: lines=15 cols=82
setlocal enabledelayedexpansion

color 0B
if not exist "%~dp0ffmpeg.exe" (
    where ffmpeg.exe >nul 2>&1
    if errorlevel 1 (
        color 0C
        echo ffmpeg.exe not found.
        echo https://www.gyan.dev/ffmpeg/builds/ - ffmpeg-git-full.7z
        echo https://github.com/BtbN/FFmpeg-Builds/releases - ffmpeg-master-latest-win64-gpl.zip
        pause
        exit /b
    )
)

echo AVIF Image Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
REM Prompt for the value of the width
set /P "width_value=Width, px (empty = auto): "
if "%width_value%"=="" set "width_value=-1"

REM Prompt for the value of the height
set /P "height_value=Height, px (empty = auto): "
if "%height_value%"=="" set "height_value=-1"

if "%width_value%"=="-1" (
    if "%height_value%"=="-1" (
        set "vf_value="
    ) else (
        set "vf_value=-vf scale=-1:%height_value%:flags=lanczos"
    )
) else (
    if "%height_value%"=="-1" (
        set "vf_value=-vf scale=%width_value%:-1:flags=lanczos"
    ) else (
        set "vf_value=-vf scale=%width_value%:%height_value%:flags=lanczos"
    )
)

REM Prompt for the value of the -cpu-used
set /P "cpu-used_value=Quality/Speed ratio modifier (from 0 to 8) (default 8): "
if "%cpu-used_value%"=="" set "cpu-used_value=8"

REM Prompt for the value of the -crf parameter
set /P "crf_value=Select the quality for constant quality mode (from -1(auto) to 63, 0 = lossless) (default 10): "
if "%crf_value%"=="" set "crf_value=10"

set color=
choice /c 01 /n /m "Convert to yuv420p pixel format (decrease size 30-50%%): no(0), yes(1)"
if errorlevel 2 set color=-pix_fmt yuv420p

choice /c 01 /n /m "Use ExifTool for metadata removal: no(0), yes(1)"
if errorlevel 2 (

    if not exist "%~dp0exiftool.exe" (
        where exiftool.exe >nul 2>&1
        if errorlevel 1 (
            color 0C
            echo exiftool.exe not found.
            echo.
            echo https://exiftool.org/install.html#Windows
            echo Rename "exiftool(-k).exe" to "exiftool.exe".
            pause
            exit /b
        )
    )

    set "use_exiftool=1"
    set "exif_folder=%~dp0exif_temp"
    if not exist "!exif_folder!" (
        mkdir "!exif_folder!"
    )
) else (
    set "use_exiftool=0"
)

set "output_folder=%~dp0avif_converted"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)

color 0E
set "converted_count=0"

for %%F in (*.jpg *.jpeg *.png *.bmp *.webp *.jxl *.avif *.heif) do (

    if %use_exiftool% equ 1 (

        set "output_name=%%~nxF"
        set "exif_file=!exif_folder!\!output_name!"

        exiftool -all= -o "!exif_file!" "%%~fF"
    )

    if %use_exiftool% equ 1 (
        ffmpeg -y -i "!exif_file!" -c:v libaom-av1 %vf_value% -cpu-used %cpu-used_value% -crf %crf_value% %color% -color_primaries bt709 -color_trc iec61966_2_1 -row-mt 1 -tiles 4x1 "!output_folder!\%%~nF.avif"
    ) else (
        ffmpeg -y -i "%%~fF" -c:v libaom-av1 %vf_value% -cpu-used %cpu-used_value% -crf %crf_value% %color% -color_primaries bt709 -color_trc iec61966_2_1 -row-mt 1 -tiles 4x1 "!output_folder!\%%~nF.avif" 
    )

    set /a "converted_count+=1"
)

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete. %converted_count% images were converted.
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  REM Open output folder
)

if exist "!exif_folder!" (
    rmdir "!exif_folder!" /s /q  REM Remove the exif_temp folder
)