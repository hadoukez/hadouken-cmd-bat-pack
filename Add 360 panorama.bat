@echo off
title "Add 360 Meta tag - equirectangular panorama"
mode con: lines=15 cols=82
setlocal enabledelayedexpansion

set "exiftool_path="

rem Check if exiftool.exe exists in the system PATH
where exiftool.exe >nul 2>&1
if %errorlevel% equ 0 (
    set "exiftool_path=exiftool.exe"
)

rem Check if exiftool.exe exists in the same folder as the script
if not defined exiftool_path (
    if exist "%~dp0exiftool.exe" (
        set "exiftool_path=%~dp0exiftool.exe"
    )
)

rem Check if exiftool.exe exists in the same folder as the input file
if not defined exiftool_path (
    if exist "%~dp1exiftool.exe" (
        set "exiftool_path=%~dp1exiftool.exe"
    )
)

rem If exiftool.exe was not found, display an error message
if not defined exiftool_path (
    color 0C
    echo exiftool.exe not found.
    echo.
    echo https://exiftool.org/install.html#Windows
    echo Rename "exiftool(-k).exe" to "exiftool.exe".
    pause
    exit /b
)

color 0B
echo "Add 360 Meta tag - equirectangular panorama"
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.

set "output_folder=%~dp1360_panorama"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)

color 0E

for %%F in (%*) do (

        set "output_name=%%~nF_360%%~xF"
        set "exif_file=!output_folder!\!output_name!"
        "%exiftool_path%" -o "!exif_file!" "%%~fF" -UsePanoramaViewer=True -ProjectionType="equirectangular"
)

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo "360 Meta tag - equirectangular panorama was added."
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  rem Open output folder
)