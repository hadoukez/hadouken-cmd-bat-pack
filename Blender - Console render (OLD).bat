@echo off
color E0
set /p name=Enter blendfile name: 
set blendfile=%name%.blend
set /p outname=Enter output file name: 
set output=%outname%_##
echo.
choice /c 12345 /m "Select cycles-device: CPU(1), CUDA(2), CUDA+CPU(3), OPENCL(4), OPENCL+CPU(5)"
if errorlevel 1 set device=CPU
if errorlevel 2 set device=CUDA
if errorlevel 3 set device=CUDA+CPU
if errorlevel 4 set device=OPENCL
if errorlevel 5 set device=OPENCL+CPU
echo You chose cycles-device: %device%
echo.
blender -b %blendfile% -o D:\MyArchive\blender\render-output\%output% -f 1 -- --cycles-device %device% --cycles-print-stats
cd D:\MyArchive\blender\render-output
start .
pause