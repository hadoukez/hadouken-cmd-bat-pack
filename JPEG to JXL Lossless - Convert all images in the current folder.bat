@echo off
title CJXL JPEG Lossless Image Converter
mode con: lines=15 cols=82
setlocal enabledelayedexpansion

if not exist "%~dp0cjxl.exe" (
    where cjxl.exe >nul 2>&1
    if errorlevel 1 (
        color 0C
        echo cjxl.exe not found.
        echo https://github.com/libjxl/libjxl/releases - jxl-x64-windows-static.zip
        pause
        exit /b
    )
)

set "output_folder=%~dp0jxl_converted"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)
color 0E
set "converted_count=0"
for %%F in (*.jpg *.jpeg) do (
    cjxl "%%~fF" "%output_folder%\%%~nF.jxl" --lossless_jpeg=1
    set /a "converted_count+=1"
)
color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete. %converted_count% images were converted.
echo.
echo CJXL JPEG Lossless Image Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  REM Open output folder
)