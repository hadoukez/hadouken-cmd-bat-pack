@echo off
title CJXL JPEG Lossless Image Converter
mode con: lines=15 cols=82
setlocal enabledelayedexpansion

set "cjxl_path="

rem Check if cjxl.exe exists in the system PATH
where cjxl.exe >nul 2>&1
if %errorlevel% equ 0 (
    set "cjxl_path=cjxl.exe"
)

rem Check if cjxl.exe exists in the same folder as the script
if not defined cjxl_path (
    if exist "%~dp0cjxl.exe" (
        set "cjxl_path=%~dp0cjxl.exe"
    )
)

rem Check if cjxl.exe exists in the same folder as the input file
if not defined cjxl_path (
    if exist "%~dp1cjxl.exe" (
        set "cjxl_path=%~dp1cjxl.exe"
    )
)

rem If cjxl.exe was not found, display an error message
if not defined cjxl_path (
    color 0C
    echo cjxl.exe not found.
    echo.
    echo Download cjxl.exe from the following sources:
    echo https://github.com/libjxl/libjxl/releases - jxl-x64-windows-static.zip
    pause
    exit /b
)

color 0B
set "output_folder=%~dp1jxl_converted"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)
color 0E
set "converted_count=0"
for %%F in (%*) do (
    "%cjxl_path%" "%%~fF" "%output_folder%\%%~nF.jxl" --lossless_jpeg=1
    set /a "converted_count+=1"
)
color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete. %converted_count% images were converted.
echo.
echo CJXL JPEG Lossless Image Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  REM Open output folder
)