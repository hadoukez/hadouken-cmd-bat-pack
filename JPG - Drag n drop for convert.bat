@echo off
title JPG Image Converter
mode con: lines=20 cols=82
setlocal enabledelayedexpansion

color 0B
echo JPG Image Converter
echo Required magick (from imagemagick v7) and cjpeg (from mozjpeg)
echo.

REM Prompt for the value of the JPG quality level
set /P "quality_value=Quality level (up to 100, empty = 88): "
if "%quality_value%"=="" set "quality_value=88"

REM Prompt for the value of the width
set /P "width_value=Width, px (empty = auto): "
if "%width_value%"=="" set "width_value=-1"

REM Prompt for the value of the height
set /P "height_value=Height, px (empty = auto): "
if "%height_value%"=="" set "height_value=-1"

REM Determine scaling options for magick
if "%width_value%"=="-1" (
    if "%height_value%"=="-1" (
        set "resize_option="
    ) else (
        set "resize_option=-resize x%height_value%"
    )
) else (
    if "%height_value%"=="-1" (
        set "resize_option=-resize %width_value%x"
    ) else (
        set "resize_option=-resize %width_value%x%height_value%"
    )
)

set "png_folder=%~dp1png_temp"
if not exist "!png_folder!" (
    mkdir "!png_folder!"
)

set "output_folder=%~dp1jpg_converted"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)

color 0E
echo.
echo Please wait.
set "converted_count=0"

for %%F in (%*) do (

    set "output_name=%%~nxF"
    set "png_file=!png_folder!\!output_name!.png"

    REM Apply resizing using the calculated option
    magick "%%~fF" -strip !resize_option! "!png_file!"

    cjpeg -quality "!quality_value!" -sample 2x2 -outfile "!output_folder!\%%~nF.jpg" "!png_file!"

    set /a "converted_count+=1"
)

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete. %converted_count% images were converted.
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  REM Open output folder
)

if exist "!png_folder!" (
    rmdir "!png_folder!" /s /q  REM Remove the png_temp folder
)
