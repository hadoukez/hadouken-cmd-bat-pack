@echo off
title JXL Image Converter
mode con: lines=15 cols=82
setlocal enabledelayedexpansion

color 0B
if not exist "%~dp0ffmpeg.exe" (
    where ffmpeg.exe >nul 2>&1
    if errorlevel 1 (
        color 0C
        echo ffmpeg.exe not found.
        echo https://www.gyan.dev/ffmpeg/builds/ - ffmpeg-git-full.7z
        echo https://github.com/BtbN/FFmpeg-Builds/releases - ffmpeg-master-latest-win64-gpl.zip
        pause
        exit /b
    )
)

echo JXL Image Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
REM Prompt for the value of the width
set /P "width_value=Width, px (empty = auto): "
if "%width_value%"=="" set "width_value=-1"

REM Prompt for the value of the height
set /P "height_value=Height, px (empty = auto): "
if "%height_value%"=="" set "height_value=-1"

if "%width_value%"=="-1" (
    if "%height_value%"=="-1" (
        set "vf_value="
    ) else (
        set "vf_value=-vf scale=-1:%height_value%:flags=lanczos"
    )
) else (
    if "%height_value%"=="-1" (
        set "vf_value=-vf scale=%width_value%:-1:flags=lanczos"
    ) else (
        set "vf_value=-vf scale=%width_value%:%height_value%:flags=lanczos"
    )
)

REM Prompt for the value of the -effort parameter
set /P "effort_value=Encoding effort (from 1 to 9, bigger = better, default 7): "
if "%effort_value%"=="" set "effort_value=7"

REM Prompt for the value of the -distance parameter
set /P "distance_value=Maximum Butteraugli distance (quality setting, lower = better, 0 = lossless, from -1(auto) to 15, default 1.2): "
if "%distance_value%"=="" set "distance_value=1.2"

choice /c 01 /n /m "Use ExifTool for metadata removal (recommended): no(0), yes(1)"
if errorlevel 2 (

    if not exist "%~dp0exiftool.exe" (
        where exiftool.exe >nul 2>&1
        if errorlevel 1 (
            color 0C
            echo exiftool.exe not found.
            echo.
            echo https://exiftool.org/install.html#Windows
            echo Rename "exiftool(-k).exe" to "exiftool.exe".
            pause
            exit /b
        )
    )

    set "use_exiftool=1"
    set "exif_folder=%~dp0exif_temp"
    if not exist "!exif_folder!" (
        mkdir "!exif_folder!"
    )
) else (
    set "use_exiftool=0"
)

set "output_folder=%~dp0jxl_converted"
if not exist "!output_folder!" (
    mkdir "!output_folder!"
)

color 0E
set "converted_count=0"

for %%F in (*.jpg *.jpeg *.png *.bmp *.webp *.jxl *.avif *.heif) do (

    if %use_exiftool% equ 1 (

        set "output_name=%%~nxF"
        set "exif_file=!exif_folder!\!output_name!"

        exiftool -all= -o "!exif_file!" "%%~fF"
    )

    if %use_exiftool% equ 1 (
        ffmpeg -y -i "!exif_file!" -c:v libjxl %vf_value% -effort %effort_value% -distance %distance_value% -modular 0 -frames:v 1 -update 1 "!output_folder!\%%~nF.jxl"
    ) else (
        ffmpeg -y -i "%%~fF" -c:v libjxl %vf_value% -effort %effort_value% -distance %distance_value% -modular 0 -frames:v 1 -update 1 "!output_folder!\%%~nF.jxl" 
    )

    set /a "converted_count+=1"
)

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete. %converted_count% images were converted.
echo.
pause

if exist "%output_folder%" (
    start "" "%output_folder%"  REM Open output folder
)

if exist "!exif_folder!" (
    rmdir "!exif_folder!" /s /q  REM Remove the exif_temp folder
)