@echo off
title NVENC H264 Video Converter - Quality preset
mode con: lines=15 cols=82

set "ffmpeg_path="

rem Check if ffmpeg.exe exists in the system PATH
where ffmpeg.exe >nul 2>&1
if %errorlevel% equ 0 (
    set "ffmpeg_path=ffmpeg.exe"
)

rem Check if ffmpeg.exe exists in the same folder as the script
if not defined ffmpeg_path (
    if exist "%~dp0ffmpeg.exe" (
        set "ffmpeg_path=%~dp0ffmpeg.exe"
    )
)

rem Check if ffmpeg.exe exists in the same folder as the input file
if not defined ffmpeg_path (
    if exist "%~dp1ffmpeg.exe" (
        set "ffmpeg_path=%~dp1ffmpeg.exe"
    )
)

rem If ffmpeg.exe was not found, display an error message
if not defined ffmpeg_path (
    color 0C
    echo ffmpeg.exe not found.
    echo.
    echo Download ffmpeg.exe from the following sources:
    echo https://www.gyan.dev/ffmpeg/builds/ - ffmpeg-git-full.7z
    echo https://github.com/BtbN/FFmpeg-Builds/releases - ffmpeg-master-latest-win64-gpl.zip
    pause
    exit /b
)

color 0B
echo NVENC H264 Video Converter
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
rem Prompt for the bitrate value
set /P "bitrate=Set video bitrate (empty = 6000k): "
if "%bitrate%"=="" set "bitrate=6000"
set "maxrate=%bitrate%*1.2"
set "bufsize=%maxrate%*2"

color 0E
"%ffmpeg_path%" -hwaccel cuda -hwaccel_output_format cuda -y -i "%~1" -c:v h264_nvenc -vf scale_cuda=format=nv12 -preset:v p7 -tune:v hq -coder:v cabac -profile:v high -spatial_aq:v 1 -temporal_aq:v 1 -rc-lookahead:v 50 -surfaces:v 60 -no-scenecut:v 1 -b_adapt:v 0 -strict_gop:v 1 -multipass:v fullres -rc:v vbr -b:v %bitrate%k -maxrate:v %maxrate%k -bufsize:v %bufsize%k -c:a aac -b:a 256k "%~dp1%~n1_h264_quality.mp4"

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete.
echo.
pause