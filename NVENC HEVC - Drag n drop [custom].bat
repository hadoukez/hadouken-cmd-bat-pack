@echo off
title NVENC HEVC Video Converter - Advanced
setlocal enabledelayedexpansion
mode con: lines=24 cols=82

set "ffmpeg_path="

rem Check if ffmpeg.exe exists in the system PATH
where ffmpeg.exe >nul 2>&1
if %errorlevel% equ 0 (
    set "ffmpeg_path=ffmpeg.exe"
)

rem Check if ffmpeg.exe exists in the same folder as the script
if not defined ffmpeg_path (
    if exist "%~dp0ffmpeg.exe" (
        set "ffmpeg_path=%~dp0ffmpeg.exe"
    )
)

rem Check if ffmpeg.exe exists in the same folder as the input file
if not defined ffmpeg_path (
    if exist "%~dp1ffmpeg.exe" (
        set "ffmpeg_path=%~dp1ffmpeg.exe"
    )
)

rem If ffmpeg.exe was not found, display an error message
if not defined ffmpeg_path (
    color 0C
    echo ffmpeg.exe not found.
    echo.
    echo Download ffmpeg.exe from the following sources:
    echo https://www.gyan.dev/ffmpeg/builds/ - ffmpeg-git-full.7z
    echo https://github.com/BtbN/FFmpeg-Builds/releases - ffmpeg-master-latest-win64-gpl.zip
    pause
    exit /b
)

:START
cls
color 0B
echo NVENC HEVC Video Converter - Advanced
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
rem Prompt for the bitrate value
set /p "bitrate=Set video bitrate (empty = 6000k): "
if "%bitrate%"=="" set "bitrate=6000"
set /a "maxrate=bitrate*12/10"
set /a "bufsize=maxrate*2"
cls

rem Prompt for the audio
set audio=
choice /c 01234 /n /m "Set audio: disable(0), source(1), aac 160k(2), aac 256k(3), custom(4)"
if errorlevel 1 set audio=-an
if errorlevel 2 set audio=-c:a copy
if errorlevel 3 set audio=-c:a aac -b:a 160k
if errorlevel 4 set audio=-c:a aac -b:a 256k
if errorlevel 5 (
    rem Prompt for the value of the audio bitrate
    echo.
    set /p "abitrate=Set audio bitrate: "
    if not defined abitrate (
        set "abitrate=160"
    )
    set "audio=-c:a aac -b:a !abitrate!k"
)
cls

rem Prompt for advanced encoder options
choice /c 01 /n /m "Edit encoder options?: no(0), yes(1)"
cls
if %errorlevel%==1 (
    rem For choice NO set next default options
    set "preset=5"
    set "tune=hq"
    set "profile=main"
    rem  can detect 10 bit video and auto set Main 10 profile
    set "pix_fmt=none"
    rem  if pix_fmt=nv12 - it's always converts 10 to 8 bits and you can't use rext
    set "level=auto"
    set "tier=high"
    set "multipass=qres"
    set "rc=vbr"
    set "lookahead=50"
    set "surfaces=60"
    set "gop=1"
    set "saq=1"
    set "taq=1"
    cls
    goto FPSSCALE
)
if %errorlevel%==2 (
    :PRESET
    color 0B
    rem Prompt for the encoding preset
    echo p1 - fastest (lowest quality)
    echo p2 - faster (lower quality)
    echo p3 - fast (low quality)
    echo p4 - medium (default)
    echo p5 - slow (good quality)
    echo p6 - slower (better quality)
    echo p7 - slowest (best quality)
    echo.
    set /p "preset=Set the encoding preset: (from 1 to 7) (empty = p5): "
    if "!preset!"=="" set "preset=5"
    if !preset! gtr 7 (
        color 0C
        echo ERROR! Enter a value from 1 to 7
        pause
        cls
        goto PRESET
    )
    cls

    :TUNE
    color 0B
    rem Prompt for the encoding tuning info
    echo hq - High quality
    echo ll - Low latency
    echo ull - Ultra low latency
    echo lossless - Lossless
    echo.
    set /p "tune=Encoding tuning info: hq(1), ll(2), ull(3), lossless(4) (empty = hq): "
    if "!tune!"=="" set "tune=1"
    if !tune! gtr 4 (
        color 0C
        echo ERROR! Enter a value from 1 to 4
        pause
        cls
        goto TUNE
    )
    cls

    :PROFILE
    color 0B
    rem Prompt for the encoding profile
    echo main - 8 bits 4:2:0
    echo main10 - 10 bits 4:2:0
    echo rext - at the moment it only works for 8 bit video, for 10 bit the Main10 profile will be applied
    echo yuv444p - 8 bits 4:4:4 - rext
    rem nv12 - 8 bits, p010le - 10 bits
    echo.
    set /p "profile=Encoding profile: main(0), main10(1), rext(2), yuv444p(3) (empty = main): "
    if "!profile!"=="" set "profile=0"
    if "!profile!"=="0" (
        set "pix_fmt=nv12"
    ) else if "!profile!"=="main" (
        set "pix_fmt=nv12"
    )
    if "!profile!"=="1" (
        set "pix_fmt=p010le"
    ) else if "!profile!"=="main10" (
        set "pix_fmt=p010le"
    )
    if "!profile!"=="2" (
        set "pix_fmt=none"
    ) else if "!profile!"=="rext" (
        set "pix_fmt=none"
    )
    if "!profile!"=="3" (
        set "pix_fmt=yuv444p"
    ) else if "!profile!"=="yuv444p" (
        set "pix_fmt=yuv444p"
    )
    if !profile! gtr 3 (
        color 0C
        echo ERROR! Enter a value from 0 to 3
        pause
        cls
        goto PROFILE
    )
    cls

    :LEVEL
    color 0B
    rem Prompt for the encoding level restriction
    echo 0 = auto
    echo 1.0    2.0    3.0    4.0    5.0    6.0
    echo        2.1    3.1    4.1    5.1    6.1
    echo                             5.2    6.2
    echo.
    set /P "level=Set the encoding level restriction: (empty = auto): "
    if "!level!"=="" set "level=0"
    if !level! gtr 6.2 (
        color 0C
        echo ERROR! Enter a value from 0 to 6.2
        pause
        cls
        goto LEVEL
    )
    cls

    :TIER
    color 0B
    rem Prompt for the encoding tier
    set /p "tier=Set the encoding tier: main(0), high(1) (empty = high): "
    if "!tier!"=="" set "tier=1"
    if !tier! gtr 1 (
        color 0C
        echo ERROR! Enter a value 0 or 1
        pause
        cls
        goto TIER
    )
    cls

    :MULTIPASS
    color 0B
    rem Prompt for the multipass encoding
    echo disabled - Single Pass
    echo qres - Two Pass encoding is enabled where first Pass is quarter resolution
    echo fullres - Two Pass encoding is enabled where first Pass is full resolution
    echo.
    set /p "multipass=Set the multipass encoding: disabled(0), qres(1), fullres(2) (empty = qres): "
    if "!multipass!"=="" set "multipass=1"
    if !multipass! gtr 2 (
        color 0C
        echo ERROR! Enter a value from 0 to 2
        pause
        cls
        goto MULTIPASS
    )
    cls

    :RC
    color 0B
    rem Prompt for the preset rate-control
    echo vbr - Variable bitrate mode
    echo cbr - Constant bitrate mode
    echo.
    set /p "rc= Override the preset rate-control: vbr(1), cbr(2) (empty = vbr): "
    if "!rc!"=="" set "rc=1"
    if !rc! gtr 2 (
        color 0C
        echo ERROR! Enter a value 1 or 2
        echo 0 is for constqp - Constant QP mode
        pause
        cls
        goto RC
    )
    cls

    :LOOKAHEAD
    color 0B
    rem Prompt for the number of frames to look ahead for rate-control
    set /p "lookahead=Set number of frames to look ahead for rate-control (empty = 50): "
    if "!lookahead!"=="" set "lookahead=50"
    set /a "surfaces=!lookahead!+10"
    if !lookahead! gtr 50 (
        color 0C
        echo ERROR! Enter a value from 0 to 50
        pause
        cls
        goto LOOKAHEAD
    )
    cls

    :GOP
    color 0B
    rem Prompt for the Strict GOP
    echo Enable to minimize GOP-to-GOP rate fluctuations
    echo.
    set /p "gop=Enable Strict GOP?: no(0), yes(1) (empty = yes): "
    if "!gop!"=="" set "gop=1"
    if !gop! gtr 1 (
        color 0C
        echo ERROR! Enter a value 0 or 1
        pause
        cls
        goto GOP
    )
    cls

    :SAQ
    color 0B
    rem Prompt for the Spatial AQ
    set /p "saq=Enable Spatial AQ?: no(0), yes(1) (empty = yes): "
    if "!saq!"=="" set "saq=1"
    if !saq! gtr 1 (
        color 0C
        echo ERROR! Enter a value 0 or 1
        pause
        cls
        goto SAQ
    )
    cls

    :TAQ
    color 0B
    rem Prompt for the Temporal AQ
    set /p "taq=Enable Temporal AQ?: no(0), yes(1) (empty = yes): "
    if "!taq!"=="" set "taq=1"
    if !taq! gtr 1 (
        color 0C
        echo ERROR! Enter a value 0 or 1
        pause
        cls
        goto TAQ
    )
    cls
)

:FPSSCALE
rem Prompt for change fps and scale
choice /c 01 /n /m "Edit fps or scale (video dimensions)?: no(0), yes(1)"
if %errorlevel%==1 (
    rem For choice NO set next default options
    set "fps_number=source_fps"
    set "format=-2"
    set "scale=0"
    set "interp=lanczos"
    cls
    goto ADD
)
if %errorlevel%==2 (
    rem Prompt for change FPS
    echo.
    echo ntsc 30000/1001 29.97
    echo ntsc_film 24000/1001 23.97
    echo pal 25
    echo film 24
    echo.
    set fps_number=
    choice /c 01234 /n /m "Select FPS: source(0), 60(1), 30(2), 15(3), custom(4)"
    if errorlevel 1 set fps_number=source_fps
    if errorlevel 2 set fps_number=60
    if errorlevel 3 set fps_number=30
    if errorlevel 4 set fps_number=15
    if errorlevel 5 (
        rem Prompt for the custom fps value
        echo.
        set /P "fps_cust=Set fps: "
        if not defined fps_cust (
            set "fps_cust=source_fps"
        )
        set "fps_number=!fps_cust!"
    )
    cls

    rem Prompt for scale
    set format=
    choice /c 012345 /n /m "Select video format: source(0), 720p(1), 1080p(2), 2k(3), 4k(4), custom(5)"
    if errorlevel 1 set format=-2
    if errorlevel 2 set format=1280
    if errorlevel 3 set format=1920
    if errorlevel 4 set format=2560
    if errorlevel 5 set format=3840
    if errorlevel 6 (
        echo.
        rem Prompt for the custom width
        set /P "format_cust=Set video width, px: "
        if not defined format_cust (
            set "format_cust=-2"
        )
        set "format=!format_cust!"
    )
    cls

    if !format! neq -2 (
        rem Prompt for cpu scale or scale_cuda
        set scale=
        echo What scale filter do you want to use?
        echo.
        echo CUDA - faster but gives worse results for donwscale. Also no crop support.
        echo CPU - can be slower but more versatile.
        echo.
        choice /c 01 /n /m "Scale filter: cuda(0), cpu(1)"
        if errorlevel 1 set scale=0
        if errorlevel 2 set scale=1
        cls

        rem Prompt for interpolation algorithm
        set interp=
        echo Select interpolation algorithm 
        choice /c 01234 /n /m "used for resizing: fast(0), nearest(1), bilinear(2), bicubic(3), lanczos(4)"
        if errorlevel 1 set interp=0
        if errorlevel 2 set interp=nearest
        if errorlevel 3 set interp=bilinear
        if errorlevel 4 set interp=bicubic
        if errorlevel 5 set interp=lanczos
        cls
    )

    if !format! equ -2 (
        set scale=0
        set interp=0
    )
)  

:ADD
rem Prompt for additional options
echo Here you can write additional options. Examples:
echo.
echo -aq-strength 8 - Spatial AQ strength scale (from 1 to 15)
echo -cq 0 - for constant quality mode in VBR rate control (from 0 to 51)
echo -bf 0 or -bf 4 - if you want to disable or change B-frames numbers
echo -intra-refresh 1 - use Periodic Intra Refresh instead of IDR frames
echo -forced-idr 1 - forcing keyframes as IDR frames
echo.
choice /c 01 /n /m "Enter additional options?: no(0), yes(1)"
if %errorlevel%==1 (
    rem For choice NO set next default options
    set "add="
    cls
    goto ADDVF
)
if %errorlevel%==2 (
    echo.
    set /p "add=Write here your additional options: "
)

:ADDVF
cls
rem Prompt for additional filter options
echo Here you can write additional filter (-vf) options.
echo (crop doesn't work with scale_cuda at present moment)
echo.
echo Write them without -vf flag.
echo If you want to use few filters - separate them by ","
echo first_filter=x,second_filter=y. Examples:
echo.
echo setpts=0.5*PTS - speed up video x2
echo setpts=2*PTS - slow down video x2
echo crop=iw:min(ih\,iw*9/16) - crop vertical video to 16:9 (only cpu scale)
echo.
choice /c 01 /n /m "Enter additional filter options?: no(0), yes(1)"
if %errorlevel%==1 (
    rem For choice NO set next default options
    set "addvf="
    cls
    goto CONFIRM
)
if %errorlevel%==2 (
    echo.
    set /p "addvf=Write here your additional filter options: "
)

:CONFIRM
color 07
rem Prompt for the confirmation
echo The video will be converted with the following options:
echo.
echo Video bitrate: !bitrate!k
if "!audio!"=="-an" (
    echo Audio: disabled
) else if "!audio!"=="-c:a copy" (
    echo Audio: source
) else if "!audio!"=="-c:a aac -b:a 160k" (
    echo Audio: aac 160k
) else if "!audio!"=="-c:a aac -b:a 256k" (
    echo Audio: aac 256k
) else if "!audio!"=="-c:a aac -b:a !abitrate!k" (
    echo Audio: aac !abitrate!k
) else ( 
    echo Audio: !audio!
)
echo Encoding preset: p!preset!
if "!tune!"=="1" (
    echo Encoding tuning: hq
) else if "!tune!"=="2" (
    echo Encoding tuning: ll
) else if "!tune!"=="3" (
    echo Encoding tuning: ull
) else if "!tune!"=="4" (
    echo Encoding tuning: lossless
) else ( 
    echo Encoding tuning: !tune!
)
if "!profile!"=="0" (
    echo Encoding profile: main - 8 bits
) else if "!profile!"=="main" (
    echo Encoding profile: auto
) else if "!profile!"=="1" (
    echo Encoding profile: main10 - 10 bits
) else if "!profile!"=="2" (
    echo Encoding profile: rext
) else if "!profile!"=="3" (
    echo Encoding profile: yuv444p - rext
) else ( 
    echo Encoding profile: !profile!
)
if "!level!"=="0" (
    echo Encoding level: auto
) else ( 
    echo Encoding level: !level!
)
if "!tier!"=="0" (
    echo Encoding tier: main
) else if "!tier!"=="1" (
    echo Encoding tier: high
) else ( 
    echo Encoding tier: !tier!
)
if "!multipass!"=="0" (
    echo Multipass encoding: disabled
) else if "!multipass!"=="1" (
    echo Multipass encoding: qres
) else if "!multipass!"=="2" (
    echo Multipass encoding: fullres
) else ( 
    echo Multipass encoding: !multipass!
)
if "!rc!"=="1" (
    echo Rate-control: vbr
) else if "!rc!"=="2" (
    echo Rate-control: cbr
) else ( 
    echo Rate-control: !rc!
)
echo Lookahead frames: !lookahead!
if "!gop!"=="0" (
    echo Strict GOP: disabled
) else if "!gop!"=="1" (
    echo Strict GOP: enabled
) else ( 
    echo Strict GOP: !gop!
)
if "!saq!"=="0" (
    echo Spatial AQ: disabled
) else if "!saq!"=="1" (
    echo Spatial AQ: enabled
) else ( 
    echo Spatial AQ: !saq!
)
if "!taq!"=="0" (
    echo Temporal AQ: disabled
) else if "!taq!"=="1" (
    echo Temporal AQ: enabled
) else ( 
    echo Temporal AQ: !taq!
)
if "!fps_number!"=="source_fps" (
    echo FPS: source
) else (
    echo FPS: !fps_number!
)
if "!format!"=="-2" (
    echo Video format: source
) else if "!format!"=="1280" (
    echo Video format: 720p
) else if "!format!"=="1920" (
    echo Video format: 1080p
) else if "!format!"=="2560" (
    echo Video format: 2k
) else if "!format!"=="3840" (
    echo Video format: 4k
) else (
    set /a "format_height=!format!/16*9"
    echo Video format: !format!x!format_height!
)
if "!scale!"=="0" (
    echo Scale filter: cuda
) else if "!scale!"=="1" (
    echo Scale filter: cpu
) else (
    echo Scale filter: !scale!
)
if "!interp!"=="0" (
    echo Interpolation algorithm: fast
) else (
    echo Interpolation algorithm: !interp!
)
if "!add!"=="" (
    echo Additional options: none
) else (
    echo Additional options: !add!
)
if "!addvf!"=="" (
    echo Additional filter options: none
) else (
    echo Additional filter options: !addvf!
)

rem Check if ffprobe.exe exists in the system PATH or in the same folder as the script
where ffprobe.exe >nul 2>&1 || exist "%~dp0ffprobe.exe" (
    rem Calculate estimated file size
    for /f %%a in ('ffprobe -v error -show_entries format^=duration -of default^=noprint_wrappers^=1:nokey^=1 "%~1"') do set "duration=%%a"
    for /f "delims=." %%b in ("!duration!") do set "duration=%%b"
    set /a "filesize=(!bitrate!+160)*!duration!/8/1024"
    echo Estimated file size: !filesize! MB
)
echo.

choice /c 01 /n /m "Convert with these options?: no(0), yes(1)"
if %errorlevel%==1 (
    set "preset="
    set "tune="
    set "profile="
    set "pix_fmt="
    set "level="
    set "tier="
    set "multipass="
    set "rc="
    set "lookahead="
    set "surfaces="
    set "gop="
    set "saq="
    set "taq="
    set "fps_number="
    set "fps_cust="
    set "format="
    set "format_cust="
    set "scale="
    set "interp="
    set "add="
    set "addvf="
    goto START
)
if %errorlevel%==2 (
    if "!scale!"=="0" (
        color 0E
        "%ffmpeg_path%" -hwaccel cuda -hwaccel_output_format cuda -y -i "%~1" ^
        -vf "fps=!fps_number!,scale_cuda=!format!:-2:interp_algo=!interp!:format=!pix_fmt!,!addvf!" ^
        -c:v hevc_nvenc -preset:v p!preset! -tune:v !tune! ^
        -level:v !level! -tier:v !tier! -profile:v !profile! ^
        -strict_gop:v !gop! -spatial_aq:v !saq! -temporal_aq:v !taq! !add! ^
        -rc-lookahead:v !lookahead! -surfaces:v !surfaces! -no-scenecut:v 1 -multipass:v !multipass! -rc:v !rc! ^
        -b:v !bitrate!k -maxrate:v !maxrate!k ^
        -bufsize:v !bufsize!k !audio! "%~dp1%~n1_hevc_custom.mp4"
    ) else (
        color 0E
        rem there is a problem with cpu scale when pix_fmt is default "none"
        if "!pix_fmt!"=="none" set "pix_fmt=nv12"
        "%ffmpeg_path%" -y -i "%~1" -pix_fmt:v !pix_fmt! ^
        -vf "fps=!fps_number!,scale=!format!:-2:flags=!interp!,!addvf!" ^
        -c:v hevc_nvenc -preset:v p!preset! -tune:v !tune! ^
        -level:v !level! -tier:v !tier! -profile:v !profile! ^
        -strict_gop:v !gop! -spatial_aq:v !saq! -temporal_aq:v !taq! !add! ^
        -rc-lookahead:v !lookahead! -surfaces:v !surfaces! -no-scenecut:v 1 -multipass:v !multipass! -rc:v !rc! ^
        -b:v !bitrate!k -maxrate:v !maxrate!k ^
        -bufsize:v !bufsize!k !audio! "%~dp1%~n1_hevc_custom.mp4"
    )
) 

color 0A
echo.
rundll32 user32.dll,MessageBeep
echo Conversion complete.
echo.
pause