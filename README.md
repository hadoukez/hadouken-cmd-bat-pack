# hadouken cmd bat pack

## How to install ffmpeg, exiftool, etc. to system?
1. Chose any folder at any place (c:\program files\ffmpeg or x:\some_folder\tools, etc)
2. Unpack ffmpeg.exe to this folder
3. Press Win+R (Run) and enter
> rundll32.exe sysdm.cpl,EditEnvironmentVariables
4.  Find Variable Path at User variables (or System if you want)
5. Press Edit and add (New) to the end path to your folder
6. OK. Done.

Now you can run ffmpeg (and my bat scripts) from any folder without copying ffmpeg.exe to every media folder you want to convert.

## ffmpeg bat files

### image converters
* AVIF - Convert all images in the current folder
* AVIF - Drag n drop for convert
* JXL - Convert all images in the current folder
* JXL - Drag n drop for convert

### video converters
* NVENC HEVC - Drag n drop \[custom\] - edit options, change fps, 720p/1080p/2k/4k presets, custom fields
* NVENC HEVC - Drag n drop \[speed\]
* NVENC HEVC - Drag n drop \[quality\]
* NVENC H264 - Drag n drop \[custom\] - edit options, change fps, 720p/1080p/2k/4k presets, custom fields
* NVENC H264 - Drag n drop \[speed\]
* NVENC H264 - Drag n drop \[quality\]
* AMF HEVC - Drag n drop \[quality\]

**ffmpeg reqired versions:**

[https://www.gyan.dev/ffmpeg/builds/](https://www.gyan.dev/ffmpeg/builds/) - [ffmpeg-git-full.7z](https://www.gyan.dev/ffmpeg/builds/ffmpeg-git-full.7z)

or

[https://github.com/BtbN/FFmpeg-Builds/releases](https://github.com/BtbN/FFmpeg-Builds/releases) - [ffmpeg-master-latest-win64-gpl.zip](https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-win64-gpl.zip)

**optional but recommended tool (especially for JXL for converting images with wide gamut color profile) - exiftool**

[https://exiftool.org/install.html#Windows](https://exiftool.org/install.html#Windows)

* Add 360 panorama  - This script uses exiftool to add an equirectangular panorama meta tag to any image.

**cjxl**

* JPEG to JXL Lossless - Convert all images in the current folder
* JPEG to JXL Lossless - Drag n drop for convert

[https://github.com/libjxl/libjxl/releases](https://github.com/libjxl/libjxl/releases) - jxl-x64-windows-static.zip


**mozjpeg**

* JPG - Drag n drop for conver

[https://mozjpeg.codelove.de/binaries.html](https://mozjpeg.codelove.de/binaries.html) - mozjpeg_4.1.1_x64.zip\mozjpeg_4.1.1_x64\shared\tools\

**ImageMagick**

[https://imagemagick.org/script/download.php#windows](https://imagemagick.org/script/download.php#windows) - ImageMagick-7.1.1-43-Q16-HDRI-x64-dll.exe


## Easy access to scripts from Windows context menu "Send to":
![sendto](screenshots/sendto.png)

Press Win+R (Run) and enter
> %APPDATA%\Microsoft\Windows\SendTo

Create here shortcuts to **Drag n drop** scripts.<br>
You can rename them and change icon.