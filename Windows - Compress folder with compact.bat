@echo off
title Compress folder
mode con: lines=25 cols=90
color 0B
choice /c 01 /n /m "What to do?: compress(0), uncompress(1)"
if errorlevel 2 (
    color 0E
    compact /u /f /s:"%~1"
    echo.
    echo Uncompression complete.
) else (
    color 0E
    compact /c /f /s:"%~1" /exe:lzx
    echo.
    echo Compression complete.
)
color 0A
rundll32 user32.dll,MessageBeep
echo.
echo More bat scripts:
echo https://gitlab.com/hadoukez/hadouken-cmd-bat-pack
echo.
pause