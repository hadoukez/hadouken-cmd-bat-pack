@echo off
color 06
echo Shut down timer. Set hours and minutes: 
echo.
set /p set_hours=How many hours? 
echo The hours were set to %set_hours%
echo.
set /p set_minutes=How many minutes? 
echo The minutes were set to %set_minutes%
echo.
set /a hours=%set_hours%*3600
set /a minutes=%set_minutes%*60
set /a seconds=%hours%+%minutes%
set start_time=%time:~0,5%
color 0A
echo Timer was started at: %date% %start_time%
echo.
echo This computer will shut down after %set_hours% hours and %set_minutes% minutes at:
powershell "(Get-Date).AddSeconds(%seconds%).ToString('dd-MMM-yy HH:mm')"
echo.
echo To cancel the timer - press Ctrl+C or close this window
timeout %seconds% /nobreak > nul
shutdown /s